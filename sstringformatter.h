/*
 * This software is licensed under the zlib open source license.
 * See COPYING for details.
 */
#ifndef __SSTRINGFORMATTER_H__
#define __SSTRINGFORMATTER_H__
#include "sstring.h"
#include <vector>
#include <stdint.h>
#include <stddef.h>

#include <stdio.h>

// build string like SString foo = SString(format).format(x).format(y)
// SString::format() returns a SStringFormatter so subsequent format() calls will
// be made on the SStringFormatter which will always return a reference to this
// once the last format() call is made the = operator will evaluate and hopefully
// this will cause the SStringFormatter::operator SString() cast to be called
// which will return the fully constructed SString

// format calls must be made by chaining or escaped sequences may get screwed up

// this is intended to be a private class and not a part of the API

// SString::format(x) can be implemented like
/*
 * { SStringFormatter f(*this); return f.format(x) } 
 */

// maybe look into useing a refcounted pointer for storing _remaining and _built to avoid some copies

class SStringFormatter
{
public:
    explicit SStringFormatter(const SString& format)
    {
        _remaining = format;
    }
    explicit SStringFormatter(SString& format)
    {
        _remaining = format;
    }
    SStringFormatter(const SStringFormatter& formatter)
    {
        _remaining = formatter._remaining;
        _built = formatter._built;
    }

    ~SStringFormatter() {}
    
    SStringFormatter& format(const SString& val);
    SStringFormatter& format(SChar val);
    SStringFormatter& format(int val);
    SStringFormatter& format(unsigned int val);
    SStringFormatter& format(long val);
    SStringFormatter& format(unsigned long val);
    SStringFormatter& format(long long val);
    SStringFormatter& format(unsigned long long val);
    SStringFormatter& format(short val);
    SStringFormatter& format(unsigned short val);
    SStringFormatter& format(double val);
    
    operator SString() const
    {
        return _built + _remaining;
    }
    
private:
    
    enum
    {
        FLAG_LEFT_JUSTIFY = 1,
        FLAG_INCLUDE_SIGN = 2,
        FLAG_SPACE_FOR_SIGN  = 4,
        FLAG_BASE_PREFIX = 8,
        FLAG_INCLUDE_DECIMAL = 8,
        FLAG_PAD_ZEROS = 16
    };
    
    enum
    {
        LENGTH_SCHAR = 1,
        LENGTH_SHORT = 2,
        LENGTH_LONG = 4,
        LENGTH_LONGLONG = 8,
        LENGTH_INTMAX = 16,
        LENGTH_UINTMAX = 32,
        LENGTH_SIZE = 64,
        LENGTH_PTRDIFF = 128,
        LENGTH_LONGDOUBLE = 256
    };
    
    template <typename T>
    void doFormat(const T& val)
    {
        size_t next = nextReplacement();
        if (next == (size_t)-1)
        {
            _built += _remaining;
            _remaining = "";
        }
        else
        {
            _built += _remaining.left(next);
            _remaining = _remaining.sub(next);
            
            
            SString replacement;
            unsigned int flags, length;
            int width, precision;
            char specifier;
            size_t len = parseFormat(flags, width, precision, length, specifier);
            if (len > 0)
            {
                SString format = _remaining.left(len);
                _remaining = _remaining.sub(len);
                
                switch (specifier)
                {
                    case 'f':
                    case 'F':
                    case 'e':
                    case 'E':
                    case 'g':
                    case 'G':
                    case 'a':
                    case 'A':
                        formatFloatingPoint(format, (double)val);
                        break;
                    case 's': 
                        // this handling will almost certianly lead to bad things but it should be consistant with printf()
                        // maybe convert to SString and use that format()
                        if (flags & LENGTH_LONG || flags & LENGTH_LONGLONG)
                            formatString(SString((const wchar_t*)(unsigned long int)val), flags, width, precision);
                        else
                            formatString(SString((const char*)(unsigned long int)val), flags, width, precision);
                        break;
                    case 'c':
                        if (flags & LENGTH_LONG || flags & LENGTH_LONGLONG)
                            formatString(SString((wchar_t)val), flags, width, precision);
                        else
                            formatString(SString((char)val), flags, width, precision);
                        break;
                    case '%':
                        _built += SChar('%');
                        doFormat<T>(val);
                        break;
                    case 'u':
                    case 'o':
                    case 'x':
                    case 'X':
                        formatUnsignedInt((uintmax_t)val, flags, width, precision, length, specifier);
                        break;
                    case 'd':
                    case 'i':
                        formatSignedInt((intmax_t)val, flags, width, precision, length);
                        break;
                }
            }
        }
    }
    
    intmax_t truncateSigned(intmax_t val, unsigned int length);
    uintmax_t truncateUnsigned(uintmax_t val, unsigned int length);
    
    
    size_t nextReplacement();
    size_t parseFormat(unsigned int& flags, int& width, int& precision, 
                          unsigned int& length, char& specifier);
    
    void formatSignedInt(intmax_t val, unsigned int flags, int width, 
                            int precision, unsigned int length);
    void formatUnsignedInt(uintmax_t val, unsigned int flags, int width, 
                             int precision, unsigned int length, char spec);
    void formatFloatingPoint(SString& format, double value);
    void formatString(const SString& val, unsigned int flags, int width, int precision);

    SString _built;
    SString _remaining;
};

// typedef provided to obscure this class in SString API
typedef SStringFormatter SString_;

#endif
