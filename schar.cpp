/*
 * This software is licensed under the zlib open source license.
 * See COPYING for details.
 */
#include "schar.h"

#include "unicodecategories.h"

#include <stdio.h>
#include <string.h>
#include <wchar.h>

#define UNKNOWN_CHAR 0xFFFD
#define ASCII_UNKNOWN_CHAR '?'

SChar SChar::_null;
const SChar& SChar::null()
{
    return _null;
}

SChar::SChar(char c)
{
    _data = (c & 1<<7 ? UNKNOWN_CHAR : c);
}

SChar::SChar(utf16_t c)
{
    // no surogate pairs allowed because we only take one char as input
    _data = ((c >= 0xD800 && c < 0xE000) ? UNKNOWN_CHAR : c);
}

SChar::SChar(utf32_t c)
{
    _data = c;
}

SChar::SChar(const SChar& c)
{
    _data = c._data;
}


SChar::SChar()
{
    _data = 0;
}

bool SChar::isDigit() const
{
    return charInCategory(_data, digitChars, numDigitRanges);
}

bool SChar::isNull() const
{
    return (_data == 0);
}

bool SChar::isPunctuation() const
{
    return charInCategory(_data, punctuationChars, numPunctuationRanges);
}

bool SChar::isWhiteSpace() const
{
    return charInCategory(_data, whitespaceChars, numWhitespaceRanges);
}

bool SChar::isUpper() const
{
    return charInCategory(_data, uppercaseChars, numUppercaseRanges);
}

bool SChar::isLower() const
{
    return charInCategory(_data, lowercaseChars, numLowercaseRanges);
}

bool SChar::isLetter() const
{
    return isUpper() || isLower();
}


SChar SChar::operator+(int v) const
{
    return SChar((utf32_t)(_data + v));
}


SChar& SChar::operator+=(int v)
{
    _data += v;
    return *this;
}

SChar SChar::operator-(int v) const
{
    return SChar((utf32_t)(_data - v));
}


SChar& SChar::operator-=(int v)
{
    _data -= v;
    return *this;
}

SChar& SChar::operator=(char c)
{
    _data = SChar(c)._data;
    return *this;
}

SChar& SChar::operator=(utf16_t c)
{
    _data = SChar(c)._data;
    return *this;
}

SChar& SChar::operator=(utf32_t c)
{
    _data = c;
    return *this;
}

char SChar::ascii() const
{
    if (_data < 255)
        return (char)_data;
    return '\0';
    //return UNKNOWN_CHAR;
}

short unsigned int SChar::local8Bit(char* buffer) const
{
    //memset(buffer, 0, MAX_8BIT);
    mbstate_t st;
    size_t ret = wcrtomb(buffer, _data, &st);
    if (ret == (size_t)-1)
    {
        //buffer[0] = UNKNOWN_CHAR;
        buffer[0] = ASCII_UNKNOWN_CHAR;
        return 1;
    }
    return ret;
}

short unsigned int SChar::utf16(utf16_t* buffer) const
{
    //memset(buffer, 0, MAX_16BIT * sizeof(utf16_t));
    if (_data > 0xFFFF)
    {
        buffer[0] = 0xD800 + ((_data - 0x10000) >> 10);
        buffer[1] = 0xDC00 + ((_data - 0x10000) & 0x3FF);
        return 2;
    }
    buffer[0] = _data;
    return 1;
}

utf32_t SChar::utf32() const
{
    return _data;
}

short unsigned int SChar::utf8(char* buffer) const
{
    //http://stackoverflow.com/questions/7695592/print-wchar-to-linux-console
    //memset(buffer, 0, MAX_8BIT);
    if (_data < (1 << 7))// 7 bit Unicode encoded as plain ascii
    {
        *buffer++ = (char)(_data);
        return 1;
    }
    else 
    if (_data < (1 << 11))// 11 bit Unicode encoded in 2 UTF-8 bytes
    {
        *buffer++ = (char)((_data >> 6) | 0xC0);
        *buffer++ = (char)((_data & 0x3F) | 0x80);
        return 2;
    }
    else 
    if (_data < (1 << 16))// 16 bit Unicode encoded in 3 UTF-8 bytes
    {
        *buffer++ = (char)((_data >> 12) | 0xE0);
        *buffer++ = (char)(((_data >> 6) & 0x3F) | 0x80);
        *buffer++ = (char)((_data & 0x3F) | 0x80);
        return 3;
    }
    else 
    if (_data < (1 << 21))// 21 bit Unicode encoded in 4 UTF-8 bytes
    {
        *buffer++ = (char)((_data >> 18) | 0xF0);
        *buffer++ = (char)(((_data >> 12) & 0x3F) | 0x80);
        *buffer++ = (char)(((_data >> 6) & 0x3F) | 0x80);
        *buffer++ = (char)((_data & 0x3F) | 0x80);
        return 4;
    }
    else
    {
        //*buffer = UNKNOWN_CHAR;
        *buffer = ASCII_UNKNOWN_CHAR;
        return 1;
    }
}
