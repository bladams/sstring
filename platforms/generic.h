
size_t utf32_strlen(const utf32_t* s)
{
    if (!s)
        return 0;
    
    size_t size = 0;
    for (; s[size]; size++)
        ;
    return size;
}

utf32_t* utf32_strstr(const utf32_t* haystack, const utf32_t* needle)
{
    return (utf32_t*)memmem(haystack, utf32_strlen(haystack), needle, utf32_strlen(needle));
}