
#include "../types.h"
#include <wchar.h>
#include <string.h>

inline size_t utf32_strlen(const utf32_t* s)
{
    return wcslen(s);
}

inline const utf32_t* utf32_strstr(const utf32_t* haystack, const utf32_t* needle)
{
    return wcsstr(haystack, needle);
}

inline double utf32_strtod(const utf32_t* str, utf32_t** endptr)
{
    return wcstod(str, endptr);
}

inline long utf32_strtol(const utf32_t* str, utf32_t** endptr, int base)
{
    return wcstol(str, endptr, base);
}

inline long long utf32_strtoll(const utf32_t* str, utf32_t** endptr, int base)
{
    return wcstoll(str, endptr, base);
}

inline unsigned long utf32_strtoul(const utf32_t* str, utf32_t** endptr, int base)
{
    return wcstoul(str, endptr, base);
}

inline unsigned long long utf32_strtoull(const utf32_t* str, utf32_t** endptr, int base)
{
    return wcstoull(str, endptr, base);
}

