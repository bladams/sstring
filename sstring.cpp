/*
 * This software is licensed under the zlib open source license.
 * See COPYING for details.
 */

#include "sstring.h"
#include "primative.h"
#include "schar.h"

#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <math.h>

#include <iconv.h>

static void warn(const char* f, ...)
{
    va_list args;
    va_start(args, f);
    vfprintf(stderr, f, args);
    va_end(args);
    
    fflush(stderr);
}

SString::SString()
{
    initNull();
}

SString::SString(const char* s)
{
    initNull();
    if (s)
        fromUtf8(s);
}

SString::SString(const utf16_t* s)
{
    initNull();
    if (s)
        fromUtf16(s);
}

SString::SString(const SChar* s)
{
    initNull();
    if (s)
        fromUtf32(s);
}

SString::SString(const utf32_t* s)
{
    initNull();
    if (s)
        fromUtf32((const SChar*)s);
}

SString::SString(const std::string& s)
{
    initNull();
    fromUtf8(s.c_str());
}

SString::SString(const SString& s)
{
    initNull();
    if (s.valid())
        fromUtf32(s._utf32);
}

SString::SString(const SChar& ch)
{
    initNull();
    _utf32 = (SChar*)malloc(2 * sizeof(SChar));
    _utf32[0] = ch.utf32();
    _utf32[1] = (utf32_t)0;
}

SString::~SString()
{
    clear();
}


size_t SString::length() const
{
    if (invalid())
        return 0;
    return utf32_strlen((const utf32_t*)_utf32);
}

bool SString::valid() const
{
    return _utf32 != NULL;
}

bool SString::invalid() const
{
    return !valid();
}

SString& SString::operator=(const SString& s)
{
    fromUtf32(s._utf32);
    return *this;
}

SString SString::operator+(const SString& s) const
{
    if (s.invalid())
        return SString(_utf32);
    
    size_t myLength = length();
    size_t otherLength = s.length();
    size_t outSize = (myLength + otherLength + 1) * sizeof(utf32_t);
    utf32_t* out = (utf32_t*)malloc(outSize);
    if (!out)
        return SString();
    
    utf32_t* ptr = out;
    memcpy(ptr, _utf32, myLength * sizeof(utf32_t));
    
    ptr += myLength;
    memcpy(ptr, s._utf32, otherLength * sizeof(utf32_t));
    
    ptr += otherLength;
    *ptr = 0;
    
    SString ret(out);
    free(out);
    return ret;
}

SString& SString::operator+=(const SString& s)
{
    setDirty();
    if (s.invalid())
        return *this;
    
    size_t myLength = length();
    size_t otherLength = s.length();
    
    size_t totalSize = (myLength + otherLength + 1) * sizeof(utf32_t);
    if (totalSize > _size)
    {
        SChar* tmp = (SChar*)realloc(_utf32, totalSize);
        if (!tmp)
        {
            clear();
            return *this;
        }
        
        _utf32 = tmp;
        _size = totalSize;
    }
    
    memcpy(_utf32 + myLength, s._utf32, (otherLength + 1) * sizeof(utf32_t));
    
    return *this;
}

bool SString::operator!() const
{
    return invalid();
}

bool SString::operator==(const SString& s) const
{
    if (length() != s.length())
        return false;
    if (invalid() && s.invalid())
        return true;
    
    return 0 == memcmp(utf32(), s.utf32(), length());
}

bool SString::operator==(const char* s) const
{
    return *this == SString(s);
}

bool SString::operator!=(const SString& s) const
{
    return !(*this == s);
}

bool SString::operator!=(const char* s) const
{
    return !(*this == s);
}

SString::operator const char*()
{
    return utf8();
}

SChar& SString::operator[](size_t index)
{
    if (invalid() || index > length())
        return (SChar&)SChar::null();
    return _utf32[index];
}

const SChar& SString::operator[](size_t index) const
{
    if (invalid() || index > length())
        return SChar::null();
    return _utf32[index];
}

const SChar& SString::at(size_t index) const
{
    if (invalid() || index > length())
        return SChar::null();
    return _utf32[index];
}

SChar& SString::set(size_t index , const SChar& val)
{
    if (invalid() || index > length())
        return (SChar&)SChar::null();
    return _utf32[index] = val;
}


long SString::find(const SString& needle, size_t start) const
{
    if (invalid() || needle.invalid() || start >= length())
        return -1;
    const utf32_t* found = utf32_strstr((const utf32_t*)(_utf32 + start), needle.utf32());
    if (!found)
        return -1;
    return (long)(found - (const utf32_t*)_utf32);
}

long SString::find(const SChar& needle, size_t start) const
{
    size_t len = length();
    if (invalid() || start >= len)
        return -1;
    
    for (size_t i = start; i < len; i++)
        if (_utf32[i] == needle)
            return (long)i;
    return -1;
}

/*
long SString::find(char needle, size_t start) const
{
    char tmp[2] = {0, 0};
    tmp[0] = needle;
    return find(SString(tmp), start);
}

long SString::find(utf16_t needle, size_t start) const
{
    utf16_t tmp[2] = {0, 0};
    tmp[0] = needle;
    return find(SString(tmp), start);
}

long SString::find(utf32_t needle, size_t start) const
{
    utf32_t tmp[2] = {0, 0};
    tmp[0] = needle;
    return find(SString(tmp), start);
}
*/

long SString::findRev(const SString& needle, size_t start) const
{
    if (invalid() || needle.invalid() || start >= length())
        return -1;
    
    const utf32_t* found = utf32_strstr_rev((const utf32_t*)_utf32, needle.utf32(), start);
    if (!found)
        return -1;
    
    long idx = (long)(found - (const utf32_t*)_utf32);
    return idx;
}

long int SString::findRev(const SChar& needle, size_t start) const
{
    size_t len = length();
    if (len < 1 || start >= length())
        return -1;
    
    for (size_t i = len - 1 - start; i >= 0; i--)
        if (_utf32[i] == needle)
            return i;
    return -1;
}


/*
long SString::findRev(char needle, size_t start) const
{
    char tmp[2] = {0, 0};
    tmp[0] = needle;
    return findRev(SString(tmp), start);
}

long SString::findRev(utf16_t needle, size_t start) const
{
    utf16_t tmp[2] = {0, 0};
    tmp[0] = needle;
    return findRev(SString(tmp), start);
}

long SString::findRev(utf32_t needle, size_t start) const
{
    utf32_t tmp[2] = {0, 0};
    tmp[0] = needle;
    return findRev(SString(tmp), start);
}
*/

bool SString::contains(const SString& needle) const
{
    return find(needle) >= 0;
}

bool SString::contains(const SChar& needle) const
{
    return find(needle) >= 0;
}

/*
bool SString::contains(char needle) const
{
    return find(needle) >= 0;
}

bool SString::contains(utf16_t needle) const
{
    return find(needle) >= 0;
}

bool SString::contains(utf32_t needle) const
{
    return find(needle) >= 0;
}
*/

bool SString::startsWith(const SString& s) const
{
    if (!valid())
        return false;
    size_t myLength = length();
    size_t otherLength = s.length();
    if (otherLength == 0 || otherLength > myLength)
        return false;
    for (size_t i = 0; i < otherLength; i++)
        if (_utf32[i] != s._utf32[i])
            return false;
    return true;
}
bool SString::startsWith(const SChar& c) const
{
    if (length() < 1)
        return false;
    return (_utf32[0] == c);
}
bool SString::endsWith(const SString& s) const
{
    if (!valid())
        return false;
    size_t myLength = length();
    size_t otherLength = s.length();
    if (otherLength == 0 || otherLength > myLength)
        return false;

    size_t j = 0;
    for (size_t i = myLength - otherLength; i < myLength; i++)
        if (_utf32[i] != s._utf32[j++])
            return false;
    return true;
}
bool SString::endsWith(const SChar& c) const
{
    size_t myLength = length();
    if (myLength < 1)
        return false;
    return  (_utf32[myLength - 1] == c);
}

SString SString::left(size_t len) const
{
    return sub(0, len);
}

SString SString::right(size_t len) const
{
    len = std::min(len, length());
    return sub(length() - len);
}

SString SString::sub(size_t begin, size_t len) const
{
    size_t myLen = length();
    if (begin >= myLen)
        return SString();
    
    len = std::min(myLen - begin, len);
    utf32_t newStr[len + 1];
    memcpy(newStr, _utf32 + begin, len * sizeof(utf32_t));
    newStr[len] = 0;
    return SString(newStr);
}

SString& SString::replace(const SString& from, const SString& to, bool all)
{
    if (invalid() || from.invalid())
        return *this;
    
    
    long start = find(from, 0);
    if (start < 0)
        return *this;
    
    setDirty();
    
    SString built = left(start);
    built += to;
    
    SString after = sub(start + from.length());
    if (all)
        built += after.replace(from, to, all);
    else
        built += after;
    
    *this = built;
    return *this;
}

SString& SString::replace(size_t start, size_t len, const SString& to)
{
    if (start >= length())
        return *this;
    
    setDirty();
    
    SString before = left(start);
    SString after = sub(start + len);
    
    *this = before + to + after;
    return *this;
}

int SString::toInt(int base, bool* ok) const
{
    bool success;
    long longVal = toLong(base, &success);
    
    // check for overflow
    if (success && sizeof(int) < sizeof(long) && 
        (longVal > (long)INT_MAX || longVal < (long)INT_MIN))
    {
        success = false;
        longVal = 0;
    }
    
    if (ok) *ok = success;
    return (int)longVal;
}

unsigned int SString::toUInt(int base, bool* ok) const
{
    bool success;
    unsigned long uLongVal = toULong(base, &success);
    
    // check for overflow
    if (success && sizeof(unsigned int) < sizeof(unsigned long) && 
        uLongVal > (unsigned long)UINT_MAX)
    {
        success = false;
        uLongVal = 0;
    }
    
    if (ok) *ok = success;
    return (unsigned int)uLongVal;
}

long SString::toLong(int base, bool* ok) const
{
    bool success = true;
    utf32_t* endptr = NULL;
    long retval = utf32_strtol((const utf32_t*)_utf32, &endptr, base);
    if (!endptr || *endptr != 0) // should be a terminator
    {
        success = false;
        retval = 0;
    }
    
    if (ok) *ok = success;
    return retval;
}

unsigned long SString::toULong(int base, bool* ok) const
{
    bool success = true;
    utf32_t* endptr = NULL;
    unsigned long retval = utf32_strtoul((const utf32_t*)_utf32, &endptr, base);
    if (!endptr || *endptr != 0) // should be a terminator
    {
        success = false;
        retval = 0;
    }
    
    if (ok) *ok = success;
    return retval;
}

long long SString::toLongLong(int base, bool* ok) const
{
    bool success = true;
    utf32_t* endptr = NULL;
    long long retval = utf32_strtoll((const utf32_t*)_utf32, &endptr, base);
    if (!endptr || *endptr != 0) // should be a terminator
    {
        success = false;
        retval = 0;
    }
    
    if (ok) *ok = success;
    return retval;
}

unsigned long long SString::toULongLong(int base, bool* ok) const
{
    bool success = true;
    utf32_t* endptr = NULL;
    unsigned long long retval = utf32_strtoull((const utf32_t*)_utf32, &endptr, base);
    if (!endptr || *endptr != 0) // should be a terminator
    {
        success = false;
        retval = 0;
    }
    
    if (ok) *ok = success;
    return retval;
}

short SString::toShort(int base, bool* ok) const
{
    bool success;
    int intVal = toInt(base, &success);
    if (success && sizeof(short) < sizeof(int) && intVal > (int)SHRT_MAX)
    {
        success = false;
        intVal = 0;
    }
    
    if (ok) *ok = success;
    return (short)intVal;
}

short unsigned int SString::toUShort(int base, bool* ok) const
{
    bool success;
    unsigned int uIntVal = toInt(base, &success);
    if (success && sizeof(short) < sizeof(int) && uIntVal > (int)USHRT_MAX)
    {
        success = false;
        uIntVal = 0;
    }
    
    if (ok) *ok = success;
    return (short)uIntVal;
}

double SString::toDouble(bool* ok) const
{
    bool success = true;
    utf32_t* endptr = NULL;
    double retVal = utf32_strtod((const utf32_t*)_utf32, &endptr);
    if (!endptr || *endptr != 0)
    {
        success = false;
        retVal = 0.0;
    }
    
    if (ok) *ok = success;
    return retVal;
}


#define LENGTH_REQUIRED(__value, __base) \
    (__value == 0 ? 2 : (floor(log10(__value)/log10(__base)) + 2))
    
static int fromUnsignedNumber(unsigned long long num, int base, bool upper, SChar** output)
{
    if (base < 2 || (base > 36 && base != 64))
        return -1;
    
    if (!output)
        return -2;
    
    char normalBaseDigitsUpper[36] = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 
        'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    char normalBaseDigitsLower[36] = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 
        'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    char base64Digits[64] = {
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 
        'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 
        'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 
        'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', 
        '4', '5', '6', '7', '8', '9', '+', '/'};
          
    
    const char* baseDigits = normalBaseDigitsLower;
    if (base == 64)
        baseDigits = base64Digits;
    else
    if (upper)
        baseDigits = normalBaseDigitsUpper;

    char* converted = (char*)malloc((int)(LENGTH_REQUIRED(num, base) + 1));
    if (!converted)
        return -3;

    size_t i = 0;
    if (num == 0)
    {
        converted[i] = 0;
        i++;
    }
    else
        while (num != 0)
        {
            converted[i] = num % base;
            num /= base;
            i++;
        }

    size_t allocated = (i+1) * sizeof(utf32_t);
    *output = (SChar*)calloc(allocated, 1);
    if (!*output)
    {
            free(converted);
            return -4;
    }

    size_t j = 0;
    for (; i > 0; i--)
        (*output)[j++] = baseDigits[(int)converted[i-1]];
    
    free(converted);
    return allocated;
}

    
SString SString::fromInt(int val, int base, bool upper)
{
    return fromLong(val, base, upper);
}

SString SString::fromUInt(unsigned int val, int base, bool upper)
{
    SString s;
    int size = fromUnsignedNumber(val, base, upper, &(s._utf32));
    if (size < 0)
        return SString();
    s._size = size;
    return s;
    
}

SString SString::fromLong(long int val, int base, bool upper)
{
    return fromLongLong(val, base, upper);
}

SString SString::fromULong(long unsigned int val, int base, bool upper)
{
    SString s;
    int size = fromUnsignedNumber(val, base, upper, &(s._utf32));
    if (size < 0)
        return SString();
    s._size = size;
    return s;
}

SString SString::fromLongLong(long long int val, int base, bool upper)
{
    bool negativeSign = val < 0 && base == 10;
    long long int absVal = (negativeSign ? -val : val);
    SString retVal = fromULongLong(absVal, base, upper);
    if (negativeSign)
        return "-" + retVal;
    else
        return retVal;
}

SString SString::fromULongLong(long long unsigned int val, int base, bool upper)
{
    SString s;
    int size = fromUnsignedNumber(val, base, upper, &(s._utf32));
    if (size < 0)
        return SString();
    s._size = size;
    return s;
}

SString SString::fromShort(short int val, int base, bool upper)
{
    return fromInt(val, base, upper);
}

SString SString::fromUShort(ushort val, int base, bool upper)
{
    SString s;
    int size = fromUnsignedNumber(val, base, upper, &(s._utf32));
    if (size < 0)
        return SString();
    s._size = size;
    return s;
}


SString SString::fromDouble(double val, unsigned int presision, char format)
{
    char fmt[256];
    snprintf(fmt, sizeof(fmt), "%%.%d%c", presision, format);
    
    //size_t valSize = presision + floor(log10(ceil(val))) + 1 + 16;
    //char buffer[valSize];
    char* buffer = NULL;
    asprintf(&buffer, fmt, val);
    //snprintf(buffer, valSize, fmt, val);
    //buffer[valSize - 1] = '\0';
    SString str(buffer);
    free(buffer);
    return str;
}


const char* SString::ascii()
{
    convertTo("ASCII//TRANSLIT", (void**)&_ascii, sizeof(char));
    return _ascii;
}

const char* SString::utf8()
{
    convertTo("UTF-8//TRANSLIT", (void**)&_utf8, sizeof(char));
    return _utf8;
}

const utf16_t* SString::utf16()
{
    convertTo("UTF-16//TRANSLIT", (void**)&_utf16, sizeof(utf16_t));
    return _utf16;
}

const utf32_t* SString::utf32() const
{
    return (const utf32_t*)_utf32;
}

void SString::initNull()
{
    _ascii = NULL;
    _utf8  = NULL;
    _utf16 = NULL;
    _utf32 = NULL;
    _local8bit = NULL;
    _size = 0;
}

void SString::setDirty()
{
    if (_ascii)
        free(_ascii);
    if (_utf8)
        free(_utf8);
    if (_utf16)
        free(_utf16);
    if (_local8bit)
        free(_local8bit);
    
    _ascii = NULL;
    _utf8  = NULL;
    _utf16 = NULL;
    _local8bit = NULL;
}

void SString::clear()
{
    setDirty();
    if (_utf32)
        free(_utf32);
    _size = 0;
    _utf32 = NULL;
}

#define INTERNAL "UTF-32LE"

void SString::convertTo(const char* conversion, void** out, size_t charSize)
{
    if (*out)
        return;
    
    iconv_t conv = iconv_open(conversion, INTERNAL);
    if (conv == (iconv_t)-1)
    {
        warn("Invalid conversion to %s: Error %d: %s\n", conversion, errno, strerror(errno));
        return;
    }
    
    size_t myLen = length();
    
    // this is a maximum size
    size_t convertSize = myLen * sizeof(utf32_t); 
    size_t remainingBuffer = convertSize;
    size_t bufferSize = convertSize + charSize;
    
    
    void* buffer = calloc(bufferSize , 1);
    if (!buffer)
        return;
    
    char* convPtr = (char*)buffer;
    
    char* inptr = (char*)_utf32;
    size_t result = iconv(conv, &inptr, &convertSize, &convPtr, &remainingBuffer);
    if (result == (size_t)-1)
    {
        warn("Conversion to %s failed: Error %d: %s\n", conversion, errno, strerror(errno));
        iconv_close(conv);
        free(buffer);
        return;
    }
    
    iconv_close(conv);
    
    size_t squishSize = (bufferSize - remainingBuffer) + charSize;
    *out = realloc(buffer, squishSize);
    if (!*out)
        free(buffer);
}

void SString::convertFrom(const char* conversion, const void* in, size_t charSize, size_t len)
{
    setDirty();
    
    size_t inSize = len * charSize;
    
    // should be at least enough space since from format can have multi code point
    // characters but utf32 cannot
    size_t bufferSize = (inSize + 1) * sizeof(utf32_t);

    if (bufferSize > _size)
    {
        clear();
        _utf32 = (SChar*)calloc(bufferSize, 1);
        if (!_utf32)
            return;
        _size = bufferSize;
    }
    else
        memset(_utf32, 0, _size * sizeof(utf32_t));
    
    
    iconv_t conv = iconv_open(INTERNAL"//TRANSLIT", conversion);
    if (conv == (iconv_t)-1)
    {
        warn("Invalid conversion from %s: Error %d: %s\n", conversion, errno, strerror(errno));
        clear();
        return;
    }
    
    SChar* ptr = _utf32;
    size_t err = iconv(conv, (char**)&in, &inSize, (char**)&ptr, &bufferSize);
    
    if (err == (size_t)-1)
    {
        warn("Conversion from %s failed: Error %d: %s\n", conversion, errno, strerror(errno));
        iconv_close(conv);
        clear();
        return;
    }
    
    iconv_close(conv);
}

void SString::fromAscii(const char* s)
{
    if (s)
    {
        convertFrom("ASCII", s, sizeof(char), strlen(s));
        _ascii = strdup(s);
    }
    else
        clear();
}

void SString::fromUtf8(const char* s)
{
    if (s)
    {
        convertFrom("UTF-8", s, sizeof(char), strlen(s));
        _utf8 = strdup(s);
    }
    else
        clear();
}

void SString::fromUtf16(const utf16_t* s)
{
    if (s)
    {
        convertFrom("UTF-16", s, sizeof(utf16_t), utf16_strlen(s));
        _utf16 = utf16_strdup(s);
    }
    else
        clear();
}

void SString::fromUtf32(const SChar* s)
{
    setDirty();
    if (!s)
    {
        clear();
        return;
    }
    
    size_t sLen = utf32_strlen((const utf32_t*)s);
    size_t sSize = (sLen + 1) * sizeof(utf32_t);
    
    if (sSize > _size)
    {
        clear();
        _utf32 = (SChar*)utf32_strdup((const utf32_t*)s);
        _size = sSize;
    }
    else
    {
        memset(_utf32, 0, _size);
        memcpy(_utf32, s, sSize);
    }
}


SString SString::leftPad(size_t length, SChar padding, bool trunc) const
{
    size_t myLength = this->length();
    if (trunc && length > myLength)
        return right(length);
    
    size_t paddingLength = length - myLength;
    utf32_t pad[paddingLength + 1];
    for (size_t i = 0; i < paddingLength; ++i)
        pad[i] = padding.utf32();
    pad[paddingLength] = 0;
    
    return SString(pad) + *this;
}

SString SString::rightPad(size_t length, SChar padding, bool trunc) const
{
    size_t myLength = this->length();
    if (trunc && length > myLength)
        return left(length);
    
    size_t paddingLength = length - myLength;
    utf32_t pad[paddingLength + 1];
    for (size_t i = 0; i < paddingLength; ++i)
        pad[i] = padding.utf32();
    pad[paddingLength] = 0;
    
    return *this + SString(pad);
}


SString_ SString::format(const SString& val) const
{
    SStringFormatter f(*this);
    return f.format(val);
}

SString_ SString::format(SChar val) const
{
    SStringFormatter f(*this);
    return f.format(val);
}

SString_ SString::format(int val) const
{
    SStringFormatter f(*this);
    return f.format(val);
}

SString_ SString::format(unsigned int val) const
{
    SStringFormatter f(*this);
    return f.format(val);
}

SString_ SString::format(long int val) const
{
    SStringFormatter f(*this);
    return f.format(val);
}

SString_ SString::format(long unsigned int val) const
{
    SStringFormatter f(*this);
    return f.format(val);
}

SString_ SString::format(long long int val) const
{
    SStringFormatter f(*this);
    return f.format(val);
}

SString_ SString::format(long long unsigned int val) const
{
    SStringFormatter f(*this);
    return f.format(val);
}

SString_ SString::format(double val) const
{
    SStringFormatter f(*this);
    return f.format(val);
}

SString_ SString::format(short int val) const
{
    SStringFormatter f(*this);
    return f.format(val);
}

SString_ SString::format(short unsigned int val) const
{
    SStringFormatter f(*this);
    return f.format(val);
}

int SString::print(FILE* f) const
{
    if (f == NULL)
        f = stdout;
    SString s(*this);
    const char* local = s.utf8();
    if (!local)
        return -1;
    return fputs(local, f);
}

int SString::printLine(FILE* f) const
{
    if (f == NULL)
        f = stdout;
    SString s(*this);
    const char* local = s.utf8();
    if (!local)
        return -1;
    int printed1 = fputs(local, f);
    if (printed1 < 0)
        return printed1;

    int printed2 = 0;
    if (!endsWith("\n"))
    {
        printed2 = fputs("\n", f);
        if (printed2 < 0)
            return printed2;
    }
    return printed1 + printed2;
}

int SString::print(const SString& s, FILE* f)
{
    return s.print(f);
}

int SString::printLine(const SString& s, FILE* f)
{
    return s.printLine(f);
}

SString operator+(const char* c, const SString& s)
{
    return SString(c) + s;
}
SString operator+(const SChar& c, const SString& s)
{
    return SString(c) + s;
}
