/*
 * This software is licensed under the zlib open source license.
 * See COPYING for details.
 */
#ifndef __SSTRING_H__
#define __SSTRING_H__

#include "types.h"
#include "schar.h"
#include <string>
class SStringFormatter;
typedef SStringFormatter SString_;
class SChar;
class SString
{
public:
    SString();
    SString(const char* s);
    SString(const utf16_t* s);
    SString(const utf32_t* s);
    SString(const SChar* s);
    SString(const std::string& s);
    SString(const SString& s);
    SString(const SChar& ch);
    
    ~SString();
    
    size_t length() const;
    bool empty() const { return length() == 0; }
    bool valid() const;
    bool invalid() const;
    
    int                toInt(int base = 10, bool* ok = NULL) const;
    unsigned int       toUInt(int base = 10, bool* ok = NULL) const;
    short              toShort(int base = 10, bool* ok = NULL) const;
    unsigned short     toUShort(int base = 10, bool* ok = NULL) const;
    long int           toLong(int base = 10, bool* ok = NULL) const;
    unsigned long      toULong(int base = 10, bool* ok = NULL) const;
    long long          toLongLong(int base = 10, bool* ok = NULL) const;
    unsigned long long toULongLong(int base = 10, bool* ok = NULL) const;
    double             toDouble(bool* ok = NULL) const;
    
    static SString fromInt(int val, int base = 10, bool upper = true);
    static SString fromUInt(unsigned int val, int base = 10, bool upper = true);
    static SString fromShort(short val, int base = 10, bool upper = true);
    static SString fromUShort(unsigned short val, int base = 10, bool upper = true);
    static SString fromLong(long val, int base = 10, bool upper = true);
    static SString fromULong(unsigned long val, int base = 10, bool upper = true);
    static SString fromLongLong(long long val, int base = 10, bool upper = true);
    static SString fromULongLong(unsigned long long val, int base = 10, bool upper = true);
    static SString fromDouble(double val, unsigned int presision = 5, char format = 'g');
    
    SString& operator=(const SString& s);
    SString operator+(const SString&s) const;
    SString& operator+=(const SString& s);
    bool operator!() const;
    bool operator==(const SString& s) const;
    bool operator==(const char* s) const;
    bool operator!=(const SString& s) const;
    bool operator!=(const char* s) const;
    operator const char*();
    
    const char* utf8();
    const char* ascii();
    const utf16_t* utf16();
    const utf32_t* utf32() const;
    
    // local 8bit encoding
    //const char* local();
    
    
    SChar& operator[](size_t index);
    const SChar& operator[](size_t index) const;
    const SChar& at(size_t index) const;
    SChar& set(size_t index, const SChar&  val);
    
    SString leftPad(size_t length, SChar padding = SChar(' '), bool truncate = false) const;
    SString rightPad(size_t length, SChar padding = SChar(' '), bool truncate = false) const ;
    
    
    long find(const SString& needle, size_t start = 0) const;
    long find(const SChar& needle, size_t start = 0) const;
    /*
    long find(char needle, size_t start = 0) const;
    long find(utf16_t needle, size_t start = 0) const;
    long find(utf32_t needle, size_t start = 0) const;
    */
    
    long findRev(const SString& needle, size_t start = 0) const;
    long findRev(const SChar& needle, size_t start = 0) const;
    /*
    long findRev(char needle, size_t start = 0) const;
    long findRev(utf16_t needle, size_t start = 0) const;
    long findRev(utf32_t needle, size_t start = 0) const;
    */
    
    bool contains(const SString& needle) const;
    bool contains(const SChar& needle) const;
    /*
    bool contains(char needle) const;
    bool contains(utf16_t needle) const;
    bool contains(utf32_t needle) const; */
    
    bool startsWith(const SString& s) const;
    bool startsWith(const SChar& c) const;
    bool endsWith(const SString& s) const;
    bool endsWith(const SChar& c) const;
    
    SString left(size_t len) const;
    SString right(size_t len) const;
    SString sub(size_t begin, size_t len = (size_t)-1) const;
    
    SString& replace(const SString& from, const SString& to, bool all = true);
    SString& replace(size_t start, size_t len, const SString& to);
    
    SString_ format(const SString& val) const;
    SString_ format(SChar val) const;
    SString_ format(int val) const;
    SString_ format(unsigned int val) const;
    SString_ format(long val) const;
    SString_ format(unsigned long val) const;
    SString_ format(long long val) const;
    SString_ format(unsigned long long val) const;
    SString_ format(short val) const;
    SString_ format(unsigned short val) const;
    SString_ format(double val) const;
    
    int print(FILE* f = NULL) const;
    int printLine(FILE* f = NULL) const;
    
    static int print(const SString& s, FILE* f = NULL);
    static int printLine(const SString& s, FILE* f = NULL);
    

private:
    void setDirty();
    void initNull();
    void clear();
    
    void fromAscii(const char* s);
    void fromUtf8(const char* s);
    void fromUtf16(const utf16_t* s);
    void fromUtf32(const SChar* s);
    void convertFrom(const char* conversion, const void* in, size_t charSize, size_t len);
    
    void convertTo(const char* conversion, void** out, size_t charSize);
    
    char* _ascii;
    char* _utf8;
    utf16_t* _utf16;
    //utf32_t* _utf32;
    SChar* _utf32;
    char* _local8bit;
    size_t _size;
};

extern SString operator+(const char* c, const SString& s);
extern SString operator+(const SChar& c, const SString& s);

#include "sstringformatter.h"

#endif
