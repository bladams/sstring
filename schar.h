
#ifndef __SCHAR_H__
#define __SCHAR_H__

#include "types.h"

#define MAX_8BIT 4
#define MAX_16BIT 2

class SString;
/*
 * This software is licensed under the zlib open source license.
 * See COPYING for details.
 */
class SChar
{
public:
    SChar(char c);
    SChar(utf16_t c);
    SChar(utf32_t c);
    SChar(const SChar& c);
    SChar();

    static const SChar& null();
    
    bool isWhiteSpace() const;
    bool isPunctuation() const;
    bool isDigit() const;
    bool isUpper() const;
    bool isLower() const;
    bool isLetter() const;
    bool isNull() const;
    
    //SChar lower();
    //SChar upper();
    
    SChar& operator=(char c);
    SChar& operator=(utf16_t c);
    SChar& operator=(utf32_t c);
    
    SChar operator+(int v) const;
    SChar operator-(int v) const;
    
    SChar& operator+=(int v);
    SChar& operator-=(int v);
    
    operator char() const { return ascii(); }
    //operator utf32_t() const { return _data; }
    //operator const utf32_t() const { return _data; }
    
    char ascii() const;
    unsigned short local8Bit(char* buffer) const;
    unsigned short utf16(utf16_t* buffer) const;
    unsigned short utf8(char* buffer) const;
    utf32_t utf32() const;
    
private:
    static SChar _null;
    utf32_t _data;
    
};

#endif
//#if sizeof(SChar) != sizeof(utf32_t)
//#error SChar != utf32_t 
//#endif

