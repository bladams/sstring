/*
 * This software is licensed under the zlib open source license.
 * See COPYING for details.
 */
#include "sstringformatter.h"
#include <stdio.h>
#include <stdlib.h>

uintmax_t SStringFormatter::truncateUnsigned(uintmax_t val, unsigned int length)
{
    switch (length)
    {
        case LENGTH_LONG:
            val = (unsigned long)val;
            break;
        case LENGTH_LONGLONG:
            val = (unsigned long long)val;
            break;
        case LENGTH_PTRDIFF:
            val = (ptrdiff_t)val;
            break;
        case LENGTH_SCHAR:
            val = (unsigned char)val;
            break;
        case LENGTH_SHORT:
            val = (unsigned short)val;
            break;
        case LENGTH_SIZE:
            val = (size_t)val;
            break;
        case LENGTH_UINTMAX:
            val = (uintmax_t)val;
            break;
        default:
            break;
    }
    return val;
}
intmax_t SStringFormatter::truncateSigned(intmax_t val, unsigned int length)
{
    switch (length)
    {
        case LENGTH_LONG:
            val = (long)val;
            break;
        case LENGTH_LONGLONG:
            val = (long long)val;
            break;
        case LENGTH_PTRDIFF:
            val = (ptrdiff_t)val;
            break;
        case LENGTH_SCHAR:
            val = (signed char)val;
            break;
        case LENGTH_SHORT:
            val = (short)val;
            break;
        case LENGTH_SIZE:
            val = (ssize_t)val;
            break;
        case LENGTH_UINTMAX:
            val = (intmax_t)val;
            break;
        default:
            break;
    }
    return val;
}

size_t SStringFormatter::nextReplacement()
{
    size_t len = _remaining.length();
    for (size_t i = 0; i < len; ++i)
    {
        if (_remaining[i] == '%')
            return i;   
    }
    return -1;
}


size_t SStringFormatter::parseFormat(unsigned int& flags, int& width, 
                          int& precision, unsigned int& length, char& specifier)
{
    const SChar* format = (SChar*)_remaining.utf32();
    
    size_t i = 0;
    if (format[i] != '%')
        return 0;
    i++;
    flags = 0;
    for (; format[i] != 0; i++)
    {
        switch (format[i])
        {
            case '-':
                flags |= FLAG_LEFT_JUSTIFY;
                break;
            case '+':
                flags |= FLAG_INCLUDE_SIGN;
                break;
            case ' ':
                flags |= FLAG_SPACE_FOR_SIGN;
                break;
            case '#':
                flags |= FLAG_BASE_PREFIX;
                break;
            case '0':
                flags |= FLAG_PAD_ZEROS;
                break;
            default:
                goto flag_loop_exit;
        }
    }
    flag_loop_exit:
    
    if (format[i] == 0)
        return 0;
 
    int w = -1;
    width =  0;
    for (; format[i] >= '0' && format[i] <= '9'; i++)
        w = (width = width * 10 + (format[i] - '0'));
    
    if (w < 0)
        width = -1;
    if (format[i] == 0)
        return 0;
    
    int p = -1;
    precision = 0;
    if (format[i] == '.')
    {
        i++;
        while (format[i] != 0 && format[i] >= '0' && format[i] <= '9')
            p = (precision = precision * 10 + (format[i++] - '0'));
    }
    
    if (p < 0)
        precision = -1;
    
    if (format[i] == 0)
        return 0;
    
    length = 0;
    switch (format[i])
    {
        case 'h':
            if (format[i + 1] == 'h')
            {
                length = LENGTH_SCHAR;
                i++;
            }
            else
                length = LENGTH_SHORT;
            break;
            
        case 'l':
            if (format[i + 1] == 'l')
            {
                length = LENGTH_LONGLONG;
                i++;
            }
            else
                length = LENGTH_LONG;
            break;
            
        case 'j':
            length = LENGTH_INTMAX;
            break;
        case 'z':
            length = LENGTH_SIZE;
            break;
        case 't':
            length = LENGTH_PTRDIFF;
            break;
        case 'L':
            length = LENGTH_LONGDOUBLE;
            break;
    }
    if (length)
        i++;
    
    if (format[i] == 0)
        return 0;
    
    // find specifier
    specifier = 0;
    switch (format[i])
    {
        case 'd':
        case 'i':
        case 'u':
        case 'o':
        case 'x':
        case 'X':
        case 'f':
        case 'F':
        case 'e':
        case 'E':
        case 'g':
        case 'G':
        case 'a':
        case 'A':
        case 'c':
        case 's':
        case 'p':
        case '%':
            specifier = format[i];
            break;
        default:
            return 0;
    }

    return i + 1;
}



static void integralToPrecision(SString& str, size_t& strlen, int precision)
{
    if (precision == 0 && str == "0")
        str = "";
    else
    if (precision > 0 && (size_t)precision > strlen)
    {
        for (; (size_t)precision > strlen; strlen++)
            str = SChar('0') + str;
    }
}

static void padToWidth(SString& str, size_t& strlen, int width, bool leftJustify, bool padZeros)
{
    if (width > 0 && (size_t)width > strlen)
    {
        SChar padding(' ');
        if (padZeros)
            padding = '0';
        
        SString filler;
        for (; (size_t)width > strlen; strlen++)
            filler += padding;
        
        if (leftJustify)
            str += filler;
        else
            str = filler + str;
    }
}

void SStringFormatter::formatSignedInt(intmax_t val, unsigned int flags, 
                                int width, int precision, unsigned int length)
{
    //val = truncateIntegralType(val, length);
    val = truncateSigned(val, length);
    
    intmax_t absVal = (val >= 0 ? val : -val);
    
    SString str = SString::fromULongLong(absVal);
    size_t strlen = str.length();
    
    integralToPrecision(str, strlen, precision);
    
    bool padFirst = flags & FLAG_PAD_ZEROS && !(flags & FLAG_LEFT_JUSTIFY);
    if (padFirst)
    {
        if (val < 0 || flags & FLAG_INCLUDE_SIGN || flags & FLAG_SPACE_FOR_SIGN)
            width--;
        padToWidth(str, strlen, width, flags & FLAG_LEFT_JUSTIFY, flags & FLAG_PAD_ZEROS);
    }
    
    if (val < 0)
    {
        str = SChar('-') + str;
        strlen++;
    }
    else
    if (flags & FLAG_INCLUDE_SIGN)
    {
        str = SChar('+') + str;
        strlen++;
    }
    else
    if (flags & FLAG_SPACE_FOR_SIGN)
    {
        str = SChar(' ') + str;
        strlen++;
    }
    if (!padFirst)
        padToWidth(str, strlen, width, flags & FLAG_LEFT_JUSTIFY, flags & FLAG_PAD_ZEROS);
    
    _built += str;
}

void SStringFormatter::formatUnsignedInt(uintmax_t val, unsigned int flags, 
                        int width, int precision, unsigned int length, char spec)
{
    bool upper = false;
    int base = 10;
    switch (spec)
    {
        case 'o':
            base = 8;
            break;
            
        case 'X':
            upper = true;
        case 'x':
            base = 16;
            break;
    }
    
    val = truncateUnsigned(val, length);
    //val = truncateIntegralType(val, length);
    SString str = SString::fromULongLong(val, base, upper);
    
    size_t strlen = str.length();
    
    integralToPrecision(str, strlen, precision);
    
    if (flags & FLAG_BASE_PREFIX && val != 0)
    {
        SString prefix;
        if (base == 8)
        {
            prefix = "0";
            strlen++;
        }
        else
        if (base == 16)
        {
            prefix = (upper ? "0X" : "0x");
            strlen += 2;
        }
        str = prefix + str;
    }
    
    if (flags & FLAG_INCLUDE_SIGN)
    {
        str = SChar('+') + str;
        strlen++;
    }
    else
    if (flags & FLAG_SPACE_FOR_SIGN)
    {
        str = SChar(' ') + str;
        strlen++;
    }
    
    padToWidth(str, strlen, width, flags & FLAG_LEFT_JUSTIFY, flags & FLAG_PAD_ZEROS);
    _built += str;
}


void SStringFormatter::formatFloatingPoint(SString& format, double value)
{
    char* buffer = NULL;
    asprintf(&buffer, format.ascii(), value);
    
    _built += buffer;
    
    free(buffer);
}


void SStringFormatter::formatString(const SString& val, unsigned int flags, 
                                       int width, int precision)
{
    SString str = val;
    if (precision >= 0)
        str = str.left(precision);
    
    size_t len = str.length();
    if (width >= 0 && (size_t)width > len)
    {
        SChar padding(' ');
        if (flags & FLAG_PAD_ZEROS)
            padding = '0';
        
        SString filling;
        for (; len < (size_t)width; len++)
            filling += padding;
        
        if (flags & FLAG_LEFT_JUSTIFY)
            str += filling;
        else
            str = filling + str;
    }
    _built += str;
}

//%[flags][width][.precision][length]specifier 


SStringFormatter& SStringFormatter::format(const SString& val)
{
    size_t next = nextReplacement();
    if (next == (size_t)-1)
    {
        _built += _remaining;
        _remaining = "";
    }
    else
    {
        _built += _remaining.left(next);
        _remaining = _remaining.sub(next);
        
        
        SString replacement;
        unsigned int flags, length;
        int width, precision;
        char specifier;
        size_t len = parseFormat(flags, width, precision, length, specifier);
        if (len > 0)
        {
            SString fmt = _remaining.left(len);
            _remaining = _remaining.sub(len);
            
            switch (specifier)
            {
                case 'f':
                case 'F':
                case 'e':
                case 'E':
                case 'g':
                case 'G':
                case 'a':
                case 'A':
                    formatFloatingPoint(fmt, val.toDouble());
                    break;
                case 's': 
                    formatString(val, flags, width, precision);
                    break;
                case 'c':
                    formatString(SString(val), flags, width, precision);
                    break;
                case '%':
                    _built += SChar('%');
                    format(val);
                    break;
                case 'u':
                case 'o':
                case 'x':
                case 'X':
                    formatUnsignedInt((uintmax_t)val.toULongLong(), flags, width, precision, length, specifier);
                    break;
                case 'd':
                case 'i':
                    formatSignedInt((intmax_t)val.toULongLong(), flags, width, precision, length);
                    break;
            }
        }
    }
    //doFormat<SString>(val);
    return *this;
}

SStringFormatter& SStringFormatter::format(int val)
{
    doFormat<int>(val);
    return *this;
}

SStringFormatter& SStringFormatter::format(unsigned int val)
{
    doFormat<unsigned int>(val);
    return *this;
}

SStringFormatter& SStringFormatter::format(long int val)
{
    doFormat<long int>(val);
    return *this;
}

SStringFormatter& SStringFormatter::format(long unsigned int val)
{
    doFormat<long unsigned int>(val);
    return *this;
}

SStringFormatter& SStringFormatter::format(long long int val)
{
    doFormat<long long int>(val);
    return *this;
}

SStringFormatter& SStringFormatter::format(long long unsigned int val)
{
    doFormat<long long unsigned int>(val);
    return *this;
}

SStringFormatter& SStringFormatter::format(double val)
{
    doFormat<double>(val);
    return *this;
}

SStringFormatter& SStringFormatter::format(short int val)
{
    doFormat<short int>(val);
    return *this;
}

SStringFormatter& SStringFormatter::format(short unsigned int val)
{
    doFormat<short int>(val);
    return *this;
}

SStringFormatter& SStringFormatter::format(SChar val)
{
    return format(SString(val));
}

