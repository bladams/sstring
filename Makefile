CXXFLAGS += -fPIC -g -Wall
LDFLAGS += -shared
LDLIBS += -lm

OBJECT_FILES = sstring.o schar.o sstringformatter.o
HEADER_FILES = sstring.h schar.h sstringformatter.h primative.h types.h unicodecategories.h platforms/linux.h

all: libsstring.so

libsstring.so : ${OBJECT_FILES} ${HEADER_FILES}
	$(CXX) $(LDFLAGS) -Wl,-soname,libsstring -o libsstring.so ${OBJECT_FILES} $(LDLIBS)
	ldconfig -n .

check: libsstring.so libsstring ${OBJECT_FILES}
	cd test && $(MAKE) check

clean:
	cd test && $(MAKE) clean
	rm -f libsstring
	rm -f *.o;
	rm -f *.so;
