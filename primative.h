/*
 * This software is licensed under the zlib open source license.
 * See COPYING for details.
 */
#include "types.h"

#include "platforms/linux.h"

#include <stdlib.h>
#include <string.h>

utf32_t* utf32_strdup(const utf32_t* s)
{
    if (!s)
        return NULL;
    
    size_t len = utf32_strlen(s);
    size_t size = (len + 1) * sizeof(utf32_t);
    
    utf32_t* cp = (utf32_t*)malloc(size);
    if (!cp)
        return NULL;
    
    return (utf32_t*)memcpy(cp, s, size);
}

void utf32_reverse(utf32_t* str)
{
    size_t len = utf32_strlen(str);
    for (size_t i = 0; i < len / 2; i++)
    {
        size_t swapPos = len - i - 1;
        utf32_t tmp = str[i];
        str[i] = str[swapPos];
        str[swapPos] = tmp;
    }
}


const utf32_t* utf32_strstr_rev(const utf32_t* haystack, const utf32_t* needle, size_t start)
{
    size_t needleLen = utf32_strlen(needle);
    size_t haystackLen = utf32_strlen(haystack);
    if (needleLen > haystackLen)
        return NULL;
    
    for (const utf32_t* searchPos = haystack + (haystackLen - needleLen - start); 
         searchPos >= haystack; searchPos--)
    {
        if (0 == memcmp(searchPos, needle, needleLen * sizeof(utf32_t)))
            return searchPos;
    }
    return NULL;
}

size_t utf16_strlen(const utf16_t* s)
{
        if (!s)
            return 0;
        
        size_t size = 0;
        for (; s[size]; size++)
            ;
        return size;
}

utf16_t* utf16_strdup(const utf16_t* s)
{
    if (!s)
        return NULL;
    
    size_t insize = (utf16_strlen(s) + 1) * sizeof(utf16_t);
    utf16_t* o = (utf16_t*)malloc(insize);
    if (!o)
        return NULL;
    
    memcpy(o, s, insize);
    return o;
}
