/*
 * This software is licensed under the zlib open source license.
 * See COPYING for details.
 */
#include "../sstring.h"
#include "../schar.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

static unsigned int passed = 0;

static void done(int retcode)
{
    printf("%u tests passed\n", passed);
    exit(retcode);
}

#define assert_true(exp) \
do { \
    if (!(exp)) { \
        printf("Assertion failed {" #exp "} Line: %d\n", __LINE__);\
        done(-1);\
    }\
    else \
        passed++; \
} while(0);
        
#define assert_false(exp) \
do { \
    if ((exp)) { \
        printf("Assertion failed !{" #exp "} Line: %d\n", __LINE__);\
        done(-1); \
    }\
    else \
        passed++; \
} while(0);

#define assert_equal(a1, a2) \
do { \
    if ((a1) != (a2)) { \
        printf("Assertion failed {" #a1 "==" #a2 "} Line: %d\n", __LINE__);\
        done(-1); \
    }\
    else \
        passed++; \
} while(0);

#define assert_not_equal(a1, a2) \
do { \
    if ((a1) == (a2)) { \
        printf("Assertion failed {" #a1 "!=" #a2 "} Line: %d\n", __LINE__);\
        done(-1); \
    }\
    else \
        passed++; \
} while(0);

#define assert_not_null(exp) \
do { \
    if ((exp) == 0) { \
        printf("Assertion failed {" #exp " != NULL} Line: %d\n", __LINE__);\
        done(-1); \
    }\
    else \
        passed++; \
} while(0);

void testChar()
{
    SChar c;
    assert_equal(c.utf32(), 0);
    assert_true(c.isNull());
    assert_false(c.isDigit())
    assert_false(c.isLetter())
    assert_false(c.isUpper())
    assert_false(c.isLower());
    assert_false(c.isPunctuation());
    assert_false(c.isWhiteSpace());
    
    c += 0x20;
    SChar c2(' ');
    assert_equal(c, c2);
    
    assert_false(c2.isNull());
    assert_false(c2.isDigit())
    assert_false(c2.isLetter())
    assert_false(c2.isUpper())
    assert_false(c2.isLower());
    assert_false(c2.isPunctuation());
    assert_true(c2.isWhiteSpace());
    
    SChar c3((utf16_t)0xfe52);
    assert_false(c3.isNull());
    assert_false(c3.isDigit())
    assert_false(c3.isLetter())
    assert_false(c3.isUpper())
    assert_false(c3.isLower());
    assert_true(c3.isPunctuation());
    assert_false(c3.isWhiteSpace());
    
    SChar c4((char)'U');
    assert_false(c4.isNull());
    assert_false(c4.isDigit())
    assert_true(c4.isLetter())
    assert_true(c4.isUpper())
    assert_false(c4.isLower());
    assert_false(c4.isPunctuation());
    assert_false(c4.isWhiteSpace());
    
    
}

void testPrinting()
{
    printf("Testing Printing\n");
    SString s("\tIf you can read this it works");
    s.printLine();
    s.printLine(stderr);
    SString::printLine(s);
    SString::printLine(s, stderr);
    
    s.print();
    printf("\n");
    s.print(stderr);
    printf("\n");
    SString::print(s);
    printf("\n");
    SString::print(s, stderr);
    printf("\n");
    
    
}

void testConversion()
{
    printf("Testing Conversions\n");
    SString wide(L"Hello1");
    
    //SString add = ascii + wide;
    printf("WIDE 2 ASCII: %s\n", wide.ascii());
    assert_not_null(wide.ascii());
    printf("WIDE 2 UTF8: %s\n", wide.utf8());
    assert_not_null(wide.utf8());
    printf("WIDE 2 UTF32: %ls\n", wide.utf32());
    assert_not_null(wide.utf32());
    printf("\n");
    
    SString ascii("Hello2");
    printf("ASCII 2 ASCII: %s\n", ascii.ascii());
    assert_not_null(ascii.ascii());
    printf("ASCII 2 UTF8: %s\n", ascii.utf8());
    assert_not_null(ascii.utf8());
    printf("ASCII 2 UTF32: %ls\n", ascii.utf32());
    assert_not_null(ascii.utf32());
    printf("\n");
}

void testOperators()
{
    printf("Testing Operators\n");
    SString s1("Hello");
    SString s2("Hello");
    // ==
    assert_equal(s1, s2);
    assert_equal(s1, "Hello");
    
    // !=
    SString s4("Goodbye");
    assert_not_equal(s1, s4);
    assert_not_equal("Hello", s4);
    
    // =
    SString s3 = s1;
    assert_equal(s1, s3);
    
    SString fromChar = "Hello";
    assert_equal(s1, fromChar);
    
    // +    
    SString add = s1 + s2;
    SString combine("HelloHello");
    SString formatted = SString("%s ==? %s").format(add).format(combine);
    assert_equal(add, combine);
    
    add = "Hello" + s1;
    assert_equal(add, combine);
    
    // +=
    SString append("Foo");
    append += SString("bar");
    assert_equal(append,"Foobar");
    
    append += "baz";
    assert_equal(append, "Foobarbaz");
    
    // const char*
    const char* cstr = (const char*)append;
    assert_true(0 == strcmp(cstr, "Foobarbaz"));
}

void testBasic()
{
    printf("Testing Basic Operations\n");
    // length()
    SString len = "12345";
    assert_true(len.length() == 5);
    
    SString null;
    assert_true(null.length() == 0);
    
    // valid()/invalid()
    assert_true(len.valid());
    assert_false(len.invalid());
    
    assert_true(null.invalid());
    assert_false(null.valid());
}

void testFind()
{
    printf("Testing Find\n");
    SString repetitive = "123 123";
    
    assert_equal(0, repetitive.find("123"));
    assert_equal(1, repetitive.find("23"));
    assert_equal(-1, repetitive.find("456"));
    assert_equal(-1, repetitive.find("1234"));
    assert_equal(-1, repetitive.find("12345678910"));
    
    long firstIndex = repetitive.find("23");
    long secondIndex = repetitive.find("23", firstIndex + 1);
    assert_equal(5, secondIndex);
    long thirdIndex = repetitive.find("23", secondIndex + 1);
    assert_equal(-1, thirdIndex);
    
    assert_equal(-1, repetitive.find("1", 10));
    
    assert_equal(2, repetitive.find('3'));
    assert_equal(3, repetitive.find((utf16_t)' '));
    assert_equal(0, repetitive.find((utf32_t)'1'));
}

void testFindRev()
{
    printf("Testing Find Rev\n");
    SString repetitive = "123 123";
    
    
    assert_equal(4, repetitive.findRev("123"));
    assert_equal(3, repetitive.findRev(" 1"));
    assert_equal(-1, repetitive.findRev("456"));
    assert_equal(-1, repetitive.findRev("1234"));
    assert_equal(-1, repetitive.findRev("12345678910"));
    
    long firstIndex = repetitive.findRev("23");
    assert_equal(5, firstIndex);
    long secondIndex = repetitive.findRev("23", firstIndex - 1);
    assert_equal(1, secondIndex);
    
    assert_equal(-1, repetitive.findRev("1", 10));
    
    assert_equal(6, repetitive.findRev('3'));
    assert_equal(3, repetitive.findRev((utf16_t)' '));
    assert_equal(4, repetitive.findRev((utf32_t)'1'));
}

void testContains()
{
    printf("Testing Contains\n");
    SString s = "abcd";
    
    assert_true(s.contains("c"));
    assert_false(s.contains("z"));
    
    assert_true(s.contains('b'));
    assert_false(s.contains('y'));
    
    assert_true(s.contains((utf16_t)'b'));
    assert_false(s.contains((utf16_t)'x'));
    
    assert_true(s.contains((utf32_t)'a'));
    assert_false(s.contains((utf32_t)'w'));
    
    assert_true(s.startsWith('a'));
    assert_true(s.startsWith((utf16_t)'a'));
    assert_true(s.startsWith("a"));
    assert_true(s.startsWith("ab"));
    assert_true(s.startsWith("abc"));
    assert_true(s.startsWith("abcd"));
    assert_false(s.startsWith("abcde"));
    
    assert_true(s.endsWith('d'));
    assert_true(s.endsWith("d"));
    assert_true(s.endsWith("cd"));
    assert_true(s.endsWith("abcd"));
    
}

void testSubString()
{
    printf("Testing Sub String\n");
    SString super = "0123456789";

    
    assert_equal(super.sub(0, 5), "01234");   
    assert_equal(super.sub(4, 2), "45");
    assert_equal(super.sub(8, 3), "89");
    assert_equal(super.sub(8), "89");
    assert_equal(super.sub(0), "0123456789");
    assert_equal(super.sub(2), "23456789");
    
    
    assert_true(super.sub(11, 2).invalid());
    assert_true(super.sub(1, 0).empty());
    
    assert_equal(super.left(3), "012");
    assert_equal(super.left(3).length(), 3);
    assert_equal(super.left(12), "0123456789");
    assert_true(super.left(0).empty());
 
    assert_equal(super.right(3), "789");
    assert_equal(super.right(10), "0123456789");
    assert_equal(super.right(12), "0123456789");
    assert_true(super.right(0).empty());
    
    assert_equal(super.left(3) + super.sub(3), super);
}

void testReplace()
{
    printf("Testing Replace\n");
    SString unique = "0123456789";
    
    assert_equal(unique.replace("1", "a"),   "0a23456789");
    assert_equal(unique.replace("23", "bc"), "0abc456789");
    assert_equal(unique.replace("z", "xyz"), "0abc456789");
    assert_equal(unique.replace("4", SString()), "0abc56789");
    
    SString repetitive = "aaaxbbbxaaaxbbb";
    assert_equal(repetitive.replace("aaa", "ccc"), "cccxbbbxcccxbbb");
    assert_equal(repetitive.replace("x", SString()), "cccbbbcccbbb");
    assert_equal(repetitive.replace("bbb", SString(), false),"ccccccbbb");
    
    SString nums =  "0123456789";
    assert_equal(nums.replace(2, 3, "AAA"),    "01AAA56789");
    assert_equal(nums.replace(4, 2, "BBB"),    "01AABBB6789");
    assert_equal(nums.replace(3, 1, SString()),"01ABBB6789"); 
    assert_equal(nums.replace(8, 3, "CCC"),    "01ABBB67CCC");
    assert_equal(nums.replace(11, 1, "DDDD"),  "01ABBB67CCC");
    assert_equal(nums.replace(1, 0, "E"),      "0E1ABBB67CCC");
    
}

#define test_format( __format , __arg ) \
do { \
    snprintf (buf, 128, __format , __arg ); \
    SString __formatted = SString( __format ).format( __arg ); \
    if (SString(buf) != __formatted)\
    { \
        printf("Assertion failed { \"%s\" == \"%ls\" } Line: %d\n", buf, __formatted.utf32(), __LINE__);\
        done(-1); \
    }\
    else \
        passed++; \
} while(0);
    

void testFormat()
{
    printf("Testing Formatting\n");
    
    char buf[256];
    SString formatted = SString("%#-34.29llX%%%s").format(42).format("hello");
    sprintf(buf, "%#-34.29llX%%%s", 42, "hello");
    assert_equal(formatted, buf);
    
    formatted = SString("%.2d%.3j%d").format(1).format(2).format(3);
    sprintf(buf, "%.2d%.3j%d", 1, 2, 3);
    assert_equal(formatted, buf);
    
    
    test_format("%d", 5);
    test_format("%d", 0);
    test_format("%.0d", 0);
    test_format("%.0d", 1);
    
    test_format("%.d", 2);
    test_format("%d", -1);
    test_format("%.3d", 5);
    
    test_format("%.3d", -5);
    test_format("%5.3d", 5);
    test_format("%-5.3d", -5);
    test_format("%hd", (short)-5);
    test_format("%hu", (unsigned short)5);
    test_format("%hu", 0xFFFFFF);
    test_format("%ld", (long)-5);
    test_format("%lu", (unsigned long)5);
    test_format("%lld", (long long)-5);
    test_format("%llu", (unsigned long long)5);
    test_format("%zd", (ssize_t)-5);
    test_format("%zu", (size_t)5);
    test_format("%-d", 5);
    test_format("%-+d", 5);
    test_format("%+-d", 5);
    test_format("%+d", -5);
    test_format("% d", 5);
    test_format("% .0d", 0);
    test_format("% +d", 5);
    test_format("%03d", 5);
    test_format("%-03d", -5);
    test_format("%3d", -5);
    test_format("%03d", -5);
    test_format("%o", 5);
    test_format("%o", 8);
    test_format("%o", 0);
    test_format("%.0o", 0);
    test_format("%.0o", 1);
    test_format("%.3o", 5);
    test_format("%.3o", 8);
    test_format("%5.3o", 5);
    test_format("%u", 5);
    test_format("%u", 0);
    test_format("%.0u", 0);
    test_format("%.0u", 1);
    test_format("%.3u", 5);
    test_format("%5.3u", 5);
    test_format("%x", 5);
    test_format("%x", 31);
    
    test_format("%x", 0);
    test_format("%.0x", 0);
    test_format("%.0x", 1);
    test_format("%.3x", 5);
    test_format("%.3x", 31);
    test_format("%5.3x", 5);
    test_format("%-x", 5);
    test_format("%03x", 5);
    test_format("%#x", 31);
    test_format("%#x", 0);
    test_format("%X", 5);
    test_format("%X", 31);
    test_format("%X", 0);
    test_format("%.0X", 0);
    test_format("%.0X", 1);
    test_format("%.3X", 5);
    test_format("%.3X", 31);
    test_format("%5.3X", 5);

    test_format("%-X", 5);
    test_format("%03X", 5);
    test_format("%#X", 31);
    test_format("%#X", 0);
    test_format("%f", M_PI);
    test_format("%.8f", M_PI);

    test_format("%.0f", M_PI);
    test_format("%1.f", M_PI);
    test_format("%3.f", M_PI);
    test_format("%+f", M_PI);
    test_format("% f", M_PI);
    test_format("%#.0f", M_PI);
    test_format("%05.2f", M_PI);
    test_format("%c", 'a');
    test_format("%.2s", "abc");
    test_format("%.6s", "abc");
    test_format("%5s", "abc");
    test_format("%-5s", "abc");
    test_format("%5.2s", "abc");
    
}

int main(int argc, char** argv)
{
    setlocale(LC_ALL, NULL);
    assert_equal(sizeof(SChar), sizeof(utf32_t));
    
    testChar();
    
    testConversion();
    testOperators();
    testBasic();
    testFind();
    testFindRev();
    testContains();
    testSubString();
    testReplace();
    testFormat();
    testPrinting();
    
    
    printf("All tests passed\n");
    done(0);
    return 0;
}